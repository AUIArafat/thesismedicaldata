﻿using System;

namespace Thesis
{
    public class YearlyTest
    {
        public int PatientId { get; set; }
        public DateTime TestDate { get; set; }
        public int TestId { get; set; }
    }
}