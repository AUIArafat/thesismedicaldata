﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestProject.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class TESTDatabaseEntities : DbContext
    {
        public TESTDatabaseEntities()
            : base("name=TESTDatabaseEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<AppointmentInfo> AppointmentInfoes { get; set; }
        public virtual DbSet<PatientInfo> PatientInfoes { get; set; }
        public virtual DbSet<ReportDetail> ReportDetails { get; set; }
        public virtual DbSet<SortedData> SortedDatas { get; set; }
        public virtual DbSet<TestImage> TestImages { get; set; }
        public virtual DbSet<MedicineInfo> MedicineInfoes { get; set; }
        public virtual DbSet<MedicinePrescription> MedicinePrescriptions { get; set; }
        public virtual DbSet<ReportResult> ReportResults { get; set; }
        public virtual DbSet<TestDetail> TestDetails { get; set; }
        public virtual DbSet<TestPrescription> TestPrescriptions { get; set; }
    }
}
