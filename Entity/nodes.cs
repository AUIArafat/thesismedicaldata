﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Thesis
{
    public class nodes
    {
        public int id { get; set; }
        public string TestName { get; set; }
        public string role { get; set; }
        public bool root { get; set; }
        //public double NormalMinValue { get; set; }
        //public double NormalMaxValue { get; set; }
        //public double ResultedValue { get; set; }
    }
    public class edges
    {
        public int source { get; set; }
        public int target { get; set; }
        public string caption { get; set; }
    }
}