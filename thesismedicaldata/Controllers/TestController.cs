﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Thesis.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        private DataAccess conn = new DataAccess();
        public ActionResult Index()
        {

            var ListOfReportData = GetAllReportDetailsForJson();
            //List<edges> edges = new List<edges>();
            //List<nodes> nodes = new List<nodes>();
            //for (int i = 1; i < ListOfReportData.Count - 1; i++)
            //{
            //    nodes nd = new nodes();
            //    if (i - 1 == 0)
            //    {
            //        nd.root = true;
            //    }
            //    nd.id = i - 1;
            //    nd.TestName = ListOfReportData[i - 1].TestName;
            //    nd.role = "Danger";
            //    nodes.Add(nd);
            //    edges ed = new edges()
            //    {
            //        source = i-1,
            //        target = i,
            //        caption = ListOfReportData[i - 1].TestName
            //    };
            //    edges.Add(ed);
            //}
            //var json = JsonConvert.SerializeObject(new
            //{
            //    comment =  "AlchemyJS contributors",
            //    nodes = nodes,
            //    edges = edges
            //});
            //var fileName = "userName";
            //fileName = String.Concat(fileName, ".json");

            //string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"\Json_File\", fileName);
            //System.IO.File.WriteAllText(destPath, json);
            //ViewBag.path = destPath;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //List<TestResult> tests = new List<TestResult>();
            //TestResult test;
            //string[] Color = new string[100];
            //var array = new Array[100];
            //var radius = 12.0;
            //var border = 2;
            //test = new TestResult()
            //{
            //    role = "Danger" + 1,
            //    color = "RED",
            //    radius = 12,
            //    borderWidth = 2
            //};
            //tests.Add(test);

            //test = new TestResult()
            //{
            //    role = "Danger" + 2,
            //    color = "GREEN",
            //    radius = 15,
            //    borderWidth = 2
            //};
            //tests.Add(test);

            //test = new TestResult()
            //{
            //    role = "Danger" + 2,
            //    color = "GREEN",
            //    radius = 15,
            //    borderWidth = 2
            //};
            //tests.Add(test);
            //var result = "";
            //foreach (var item in tests)
            //{
            //    result += "\"" + item.role + "\": {\"color\": \"" + item.color + "\",\"radius\": " + item.radius + ",\"borderWidth\": " + item.borderWidth + "},";
            //}
            //ViewBag.Data = result;
            //return View(ListOfReportData);

            //+++++++++++++++++++++++++++++++++++
            return View();
        }

        private List<ReportDetails> GetAllReportDetailsForJson()
        {
            List<ReportDetails> ListForJson = conn.SelectReportDetails("SELECT *FROM ReportDetails");
            return ListForJson;
        }
        public ActionResult TempTest()
        {
            return View();
        }
        public ActionResult TestRelation()
        {
            return View();
        }
        public ActionResult PartialYearlyTest(int TestId, int PatientId)
        {
            return PartialView();
        }
        public ActionResult yearlyTest()
        {
            int PatientId = 1;
            int TestId = 12;
            string sql = "";
            sql = "select RD.* from ReportDetails as RD " +
                "left join SortedData as SD on RD.Id = SD.ReportId" +
                " left join YearlyTest as yt on RD.TestId = yt.TestId" +
                " where yt.patientid = '" + PatientId + "' AND RD.TestId = '" + TestId + "' AND SD.ResultedDiff > 0" +
                " Order by SD.ResultedDiff DESc";

            ViewBag.Dangers = conn.SelectReportDetails(sql);
            sql = "select RD.* from ReportDetails as RD " +
              " left join SortedData as SD on RD.Id = SD.ReportId" +
              " left join YearlyTest as yt on RD.TestId = yt.TestId" +
              " where yt.patientid = '" + PatientId + "' AND RD.TestId = '" + TestId + "' AND SD.ResultedDiff < 0" +
              " Order by SD.ResultedDiff DESc";
            ViewBag.Safe = conn.SelectReportDetails(sql);

            sql = "select RD.* from ReportDetails as RD " +
              " left join SortedData as SD on RD.Id = SD.ReportId" +
              " left join YearlyTest as yt on RD.TestId = yt.TestId" +
              " where yt.patientid = '" + PatientId + "' AND RD.TestId = '" + TestId + "' AND SD.ResultedDiff = 0" +
              " Order by SD.ResultedDiff DESc";
            ViewBag.Warning = conn.SelectReportDetails(sql);
            int year = DateTime.Now.Year;
            DateTime firstDay = new DateTime(year, 1, 1);
            List<YearlyTest> yearlyTest = conn.GetYearlyTest("select *from YearlyTest where PatientId=1 AND TestDate > '" + firstDay + "'");
            return View(yearlyTest);
        }

        public ActionResult GetReport(int id)
        {
            int PatientId = 1;
            int TestId = id;
            string sql = "";
            sql = "select RD.* from ReportDetails as RD " +
                "left join SortedData as SD on RD.Id = SD.ReportId" +
                " left join YearlyTest as yt on RD.TestId = yt.TestId" +
                " where yt.patientid = '" + PatientId + "' AND RD.TestId = '" + TestId + "' AND SD.ResultedDiff > 0" +
                " Order by SD.ResultedDiff DESc";

            List<ReportDetails> DangerList = conn.SelectReportDetails(sql);
            List<FinalView> Danger = GetConvertedReportDetails(DangerList);
            sql = "select RD.* from ReportDetails as RD " +
              " left join SortedData as SD on RD.Id = SD.ReportId" +
              " left join YearlyTest as yt on RD.TestId = yt.TestId" +
              " where yt.patientid = '" + PatientId + "' AND RD.TestId = '" + TestId + "' AND SD.ResultedDiff < 0" +
              " Order by SD.ResultedDiff DESc";
            List<ReportDetails> SafeList = conn.SelectReportDetails(sql);
            List<FinalView> Safe = GetConvertedReportDetails(SafeList);
            sql = "select RD.* from ReportDetails as RD " +
              " left join SortedData as SD on RD.Id = SD.ReportId" +
              " left join YearlyTest as yt on RD.TestId = yt.TestId" +
              " where yt.patientid = '" + PatientId + "' AND RD.TestId = '" + TestId + "' AND SD.ResultedDiff = 0" +
              " Order by SD.ResultedDiff DESc";
            List<ReportDetails> WarningList = conn.SelectReportDetails(sql);
            List<FinalView> Warning = GetConvertedReportDetails(WarningList);
            return Json(new { danger = Danger, safe = Safe, warning = Warning }, JsonRequestBehavior.AllowGet);
        }

        private ReportDetails Conversion(double NormalValueMax, double NormalValueMin, double ResultedValue, int ReportId)
        {
            ReportDetails reportDetails = new ReportDetails();
            reportDetails.OrgNormalMaxValue = NormalValueMax;
            reportDetails.OrgNormalMinValue = NormalValueMin;
            reportDetails.OrgResultedValue = ResultedValue;
            double MaxValue = 0;
            double ResultedDiff;
            int countrow;
            if (NormalValueMax == ResultedValue || NormalValueMin == ResultedValue)
            {
                MaxValue = NormalValueMax;
                ResultedValue = (100 * ResultedValue) / NormalValueMax;
                NormalValueMin = (100 * NormalValueMin) / NormalValueMax;
                NormalValueMax = 100;
                ResultedDiff = 0;
                countrow = conn.SelectSortedData("SELECT *FROM SortedData where ReportId='" + ReportId + "'").Count;
                if (countrow == 1)
                    conn.Update("UPDATE sortedData SET ResultedDiff='" + ResultedDiff + "' WHERE ReportId='" + ReportId + "'");
                else
                    conn.Insert("INSERT INTO sorteddata (ResultedDiff, ReportId) VALUES ('" + ResultedDiff + "', '" + ReportId + "')");
            }
            else if (NormalValueMax > ResultedValue && ResultedValue > NormalValueMin)
            {
                MaxValue = NormalValueMax;
                ResultedValue = (100 * ResultedValue) / NormalValueMax;
                NormalValueMin = (100 * NormalValueMin) / NormalValueMax;
                NormalValueMax = 100;
                ResultedDiff = ((NormalValueMax - NormalValueMin) / 2) - ResultedValue;
                countrow = conn.SelectSortedData("SELECT *FROM SortedData where ReportId='" + ReportId + "'").Count;
                if (countrow == 1)
                    conn.Update("UPDATE sortedData SET ResultedDiff='" + ResultedDiff + "' WHERE ReportId='" + ReportId + "'");
                else
                    conn.Insert("INSERT INTO sorteddata (ResultedDiff, ReportId) VALUES ('" + ResultedDiff + "', '" + ReportId + "')");
            }
            else if (ResultedValue < NormalValueMin)
            {
                MaxValue = NormalValueMin;
                ResultedValue = (100 * ResultedValue) / NormalValueMax;
                NormalValueMin = (100 * NormalValueMin) / NormalValueMax;
                NormalValueMax = 100;
                ResultedDiff = NormalValueMin - ResultedValue;
                countrow = conn.SelectSortedData("SELECT *FROM SortedData where ReportId='" + ReportId + "'").Count;
                if (countrow == 1)
                    conn.Update("UPDATE sortedData SET ResultedDiff='" + ResultedDiff + "' WHERE ReportId='" + ReportId + "'");
                else
                    conn.Insert("INSERT INTO sorteddata (ResultedDiff, ReportId) VALUES ('" + ResultedDiff + "', '" + ReportId + "')");

                MaxValue = NormalValueMin;
                ResultedValue = (100 * ResultedValue) / NormalValueMin;
                NormalValueMax = (100 * NormalValueMax) / NormalValueMin;
                NormalValueMin = 100;
                if (ResultedValue >= 0)
                    ResultedValue = 100 - ResultedValue;
                ResultedValue = Math.Abs(ResultedValue);
                if (ResultedValue >= 100)
                    ResultedValue = 100 - 20;
            }
            else
            {
                MaxValue = ResultedValue;
                NormalValueMax = (100 * NormalValueMax) / ResultedValue;
                NormalValueMin = (100 * NormalValueMin) / ResultedValue;
                ResultedValue = 100;
                ResultedDiff = ResultedValue - NormalValueMax;
                countrow = conn.SelectSortedData("SELECT *FROM SortedData where ReportId='" + ReportId + "'").Count;
                if (countrow == 1)
                    conn.Update("UPDATE sortedData SET ResultedDiff='" + ResultedDiff + "' WHERE ReportId='" + ReportId + "'");
                else
                    conn.Insert("INSERT INTO sorteddata (ResultedDiff, ReportId) VALUES ('" + ResultedDiff + "', '" + ReportId + "')");

            }
            NormalValueMax = Math.Abs(NormalValueMax);
            NormalValueMin = Math.Abs(NormalValueMin);
            ResultedValue = Math.Abs(ResultedValue);

            //##Deduct Extra Value

            //##Add Some Extra Value
            if (ResultedValue < 20)
                ResultedValue += 20;
            if (NormalValueMin < 20)
                NormalValueMin += 20;
            if (NormalValueMax < 20)
                NormalValueMax += 20;
            reportDetails.NormalMaxValue = (NormalValueMax);
            reportDetails.NormalMinValue = (NormalValueMin);
            reportDetails.ResultedValue = (ResultedValue);

            return reportDetails;
        }

        private List<FinalView> GetConvertedReportDetails(List<ReportDetails> list)
        {
            //List<ReportDetails> list = conn.SelectReportDetails("SELECT * FROM ReportDetails as RD Left JOIN SortedData as sd ON sd.ReportId = RD.Id order by sd.ResultedDiff DESC");
            List<FinalView> convertedList = new List<FinalView>();
            foreach (var item in list)
            {
                ReportDetails report = Conversion(item.NormalMaxValue, item.NormalMinValue, item.ResultedValue, item.Id);
                report.TestName = item.TestName;
                if (report.OrgResultedValue == report.OrgNormalMaxValue || report.OrgResultedValue == report.OrgNormalMinValue)
                {
                    FinalView finalView = new FinalView()
                    {
                        Id = item.Id,
                        TestName = item.TestName,
                        OuterRadius = 100,
                        InnerRadius = 98,
                        //InnerColor = (report.ResultedValue * 255).ToString(),
                        //OuterColor = (report.NormalMaxValue * 255).ToString()
                        InnerColor = "#FFFF00",
                        //OuterColor = "#00" + ((Convert.ToInt32(report.NormalMaxValue) * 255)/100).ToString("X") + "00",
                        OuterColor = "#FFFF00",
                        OrgResultedValue = report.OrgResultedValue,
                        OrgNormalMinValue = report.OrgNormalMinValue,
                        OrgNormalMaxValue = report.OrgNormalMaxValue
                    };
                    convertedList.Add(finalView);
                }
                else if (item.NormalMaxValue > item.ResultedValue && item.NormalMinValue < item.ResultedValue)
                {
                    var hex = ((Convert.ToInt32(Math.Abs(report.ResultedValue - report.NormalMaxValue)) * 255) / 100).ToString("X");
                    if (hex.Length <= 1)
                    {
                        hex = "0" + hex;
                    }
                    FinalView finalView = new FinalView()
                    {
                        Id = item.Id,
                        TestName = item.TestName,
                        OuterRadius = report.NormalMaxValue,
                        InnerRadius = report.ResultedValue,
                        //InnerColor = (report.ResultedValue * 255).ToString(),
                        //OuterColor = (report.NormalMaxValue * 255).ToString()
                        InnerColor = "#00" + hex + "00",
                        //OuterColor = "#00" + ((Convert.ToInt32(report.NormalMaxValue) * 255)/100).ToString("X") + "00",
                        OuterColor = "white",
                        OrgResultedValue = report.OrgResultedValue,
                        OrgNormalMinValue = report.OrgNormalMinValue,
                        OrgNormalMaxValue = report.OrgNormalMaxValue
                    };
                    convertedList.Add(finalView);
                    //myCircle(X, Y, normalValueMax, c3, c2);
                    // myCircle(X, Y, rV, c2,c3 )
                }
                else if (item.ResultedValue < item.NormalMinValue)
                {
                    var hex = ((Convert.ToInt32(Math.Abs(report.NormalMinValue)) * 255) / 100).ToString("X");
                    if (hex.Length <= 1)
                    {
                        hex = "0" + hex;
                    }
                    FinalView finalView = new FinalView()
                    {
                        Id = item.Id,
                        TestName = item.TestName,
                        OuterRadius = report.NormalMinValue,
                        InnerRadius = report.ResultedValue,
                        //InnerColor = (report.ResultedValue * 255).ToString(),
                        //OuterColor = (report.NormalMinValue * 255).ToString()
                        InnerColor = "#" + hex + "0000",
                        //OuterColor = "#00" + ((Convert.ToInt32(report.NormalMinValue) * 255) / 100).ToString("X") + "00",
                        OuterColor = "white",
                        OrgResultedValue = report.OrgResultedValue,
                        OrgNormalMinValue = report.OrgNormalMinValue,
                        OrgNormalMaxValue = report.OrgNormalMaxValue
                    };
                    convertedList.Add(finalView);
                    //myCircle(X, Y, normalValueMin, c3, c2)
                    //myCircle(X, Y, rV, c2, c1)
                }

                else
                {
                    var hex = ((Convert.ToInt32(Math.Abs(report.NormalMaxValue)) * 255) / 100).ToString("X");
                    if (hex.Length <= 1)
                    {
                        hex = "0" + hex;
                    }
                    FinalView finalView = new FinalView()
                    {
                        Id = item.Id,
                        TestName = item.TestName,
                        OuterRadius = report.ResultedValue,
                        InnerRadius = report.NormalMaxValue,
                        //InnerColor = (report.NormalMaxValue * 255).ToString(),
                        //OuterColor = (report.ResultedValue * 255).ToString()
                        //InnerColor = "#00" + ((Convert.ToInt32(report.NormalMaxValue) * 255) / 100).ToString("X") + "00",
                        InnerColor = "white",
                        //OuterColor = "red",
                        OuterColor = "#" + hex + "0000",
                        OrgResultedValue = report.OrgResultedValue,
                        OrgNormalMinValue = report.OrgNormalMinValue,
                        OrgNormalMaxValue = report.OrgNormalMaxValue
                    };
                    convertedList.Add(finalView);
                    // myCircle(X, Y, rV, c2, c1)
                    //myCircle(X, Y, normalValueMax, c1, c2)
                }

            }
            return convertedList;
        }
    }
}