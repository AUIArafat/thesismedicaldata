//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestProject.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MedicineInfo
    {
        public System.Guid MedicineId { get; set; }
        public string MedicineName { get; set; }
        public Nullable<int> Weight { get; set; }
    }
}
