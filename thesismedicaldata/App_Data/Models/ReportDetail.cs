//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestProject.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReportDetail
    {
        public int Id { get; set; }
        public Nullable<int> TestId { get; set; }
        public string TestName { get; set; }
        public double NormalMinValue { get; set; }
        public double NormalMaxValue { get; set; }
        public double ResultedValue { get; set; }
    }
}
