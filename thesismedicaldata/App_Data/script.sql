USE [master]
GO
/****** Object:  Database [medicaldata]    Script Date: 7/15/2018 10:30:45 AM ******/
CREATE DATABASE [medicaldata]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'medicaldata', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\medicaldata.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'medicaldata_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\medicaldata_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [medicaldata] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [medicaldata].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [medicaldata] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [medicaldata] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [medicaldata] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [medicaldata] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [medicaldata] SET ARITHABORT OFF 
GO
ALTER DATABASE [medicaldata] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [medicaldata] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [medicaldata] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [medicaldata] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [medicaldata] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [medicaldata] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [medicaldata] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [medicaldata] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [medicaldata] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [medicaldata] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [medicaldata] SET  DISABLE_BROKER 
GO
ALTER DATABASE [medicaldata] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [medicaldata] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [medicaldata] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [medicaldata] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [medicaldata] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [medicaldata] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [medicaldata] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [medicaldata] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [medicaldata] SET  MULTI_USER 
GO
ALTER DATABASE [medicaldata] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [medicaldata] SET DB_CHAINING OFF 
GO
ALTER DATABASE [medicaldata] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [medicaldata] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [medicaldata]
GO
/****** Object:  Table [dbo].[ReportDetails]    Script Date: 7/15/2018 10:30:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReportDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TestId] [int] NULL,
	[TestName] [varchar](500) NOT NULL,
	[NormalMinValue] [float] NOT NULL,
	[NormalMaxValue] [float] NOT NULL,
	[ResultedValue] [float] NOT NULL,
 CONSTRAINT [PK_ReportDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SortedData]    Script Date: 7/15/2018 10:30:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SortedData](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ResultedDiff] [float] NOT NULL,
	[ReportId] [int] NOT NULL,
 CONSTRAINT [PK_SortedData] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TestImage]    Script Date: 7/15/2018 10:30:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TestImage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ImagePath] [varchar](max) NOT NULL,
	[ReportId] [int] NOT NULL,
 CONSTRAINT [PK_TestImage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[YearlyTest]    Script Date: 7/15/2018 10:30:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[YearlyTest](
	[PatientId] [int] NULL,
	[TestDate] [datetime] NULL,
	[TestId] [int] NULL
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[ReportDetails] ON 

INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (65, 12, N'Blood Test', 0.3, 5.6, 12)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (66, 12, N'Urin Test', 0.1, 5.6, 4)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (67, 12, N'Urin Test', 30, 40, 500)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (68, 12, N'Urin Test', 40, 100, 60)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (69, 12, N'Urin Test', 0.1, 5.6, 5.6)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (70, 12, N'S. Total Prottein', 6.6, 8.7, 6.6)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (71, 12, N'S. Albumin', 3.8, 5.1, 3.5)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (72, 13, N'S. Globulin', 2.5, 3.6, 3.2)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (73, 13, N'S. Amylase', 3, 90, 120)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (74, 13, N'S. Urin', 10, 50, 23)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (75, 13, N'S. Calcium', 8.1, 10.4, 7.9)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (76, 13, N'Hb%', 12, 17, 16.1)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (77, 13, N'ESR: (Auto analyzer)', 1, 10, 12)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (78, 13, N'Red Blood Cells', 3.8, 4.5, 5.49)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (79, 15, N'White Blood Cells', 4, 11, 8.5)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (80, 15, N'Platelets Count', 150, 450, 233)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (81, 15, N'Neutrophils', 40, 75, 60)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (82, 15, N'Lymphocytes', 20, 40, 30)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (83, 15, N'Monocytes', 2, 10, 6)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (84, 15, N'Eosinophils', 1, 6, 4)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (85, 16, N'P.C.V/HCT', 36, 48, 46.7)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (86, 16, N'M.C.V', 80, 95, 85.1)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (87, 16, N'M.C.H', 27, 34, 29.3)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (88, 16, N'M.C.H.C', 30, 35, 34.5)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (89, 16, N'R.D.W-S.D', 39, 46, 38.2)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (90, 16, N'R.D.W-C.V', 11, 14, 12.2)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (91, 14, N'M.P.V', 7.2, 9.2, 10.7)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (92, 14, N'T.Circulating eosinophils', 0.02, 0.5, 0.34)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (93, 14, N'Hb%', 12, 17, 50.3)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (94, 14, N'ESR: (Auto analyzer)', 1, 10, 20)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (95, 14, N'Red Blood Cells', 3.8, 4.5, 1.5)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (96, 33, N'White Blood Cells', 4, 11, 4.5)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (97, 34, N'Platelets Count', 150, 450, 140)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (98, 35, N'Neutrophils', 40, 75, 70)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (99, 36, N'Lymphocytes', 20, 40, 35)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (100, 37, N'Monocytes', 2, 10, 11)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (101, 38, N'Eosinophils', 1, 6, 3)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (102, 39, N'P.C.V/HCT', 36, 48, 47)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (103, 40, N'M.C.V', 80, 95, 95)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (104, 41, N'M.C.H', 27, 34, 40.1)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (105, 42, N'M.C.H.C', 30, 35, 34.5)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (106, 43, N'R.D.W-S.D', 39, 46, 15.1)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (107, 44, N'R.D.W-C.V', 11, 14, 50)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (108, 45, N'M.P.V', 7.2, 9.2, 11.3)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (109, 46, N'T.Circulating eosinophils', 0.02, 0.5, 0.34)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (110, 47, N'Blood Test', 0.3, 5.6, 6)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (111, 48, N'Urin Test', 0.1, 5.6, 5.6)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (112, 49, N'Urin Test', 30, 40, 100)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (113, 50, N'S. Total Prottein', 6.6, 8.7, 20.3)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (116, 27, N'Hb%', 12, 17, 23)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (117, 27, N'Red Blood Cells', 3.8, 4.5, 5)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (118, 28, N'White Blood Cells', 4, 11, 15)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (119, 28, N'Urin Test', 30, 40, 10)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (120, 28, N'M.V.P', 7.2, 9.2, 8)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (121, 29, N'R.D.W-C.V', 11, 14, 12)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (122, 29, N'Lymphocytes', 20, 40, 50)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (123, 29, N'Eosinophils', 1, 6, 9)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (124, 29, N'P.C.V/HCT', 36, 48, 38)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (125, 30, N'Blood Test', 0.3, 5.6, 7)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (126, 30, N'S. Total Prottein', 6.6, 8.7, 9)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (128, 30, N'T.Circutating eosinophils', 0.02, 0.5, 0.6)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (129, 31, N'Neutrophils', 40, 75, 65)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (130, 31, N'Hb%', 12, 17, 17)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (131, 31, N'M.V.P', 7.2, 9.2, 11.3)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (133, 47, N'M.C.H', 27, 34, 25)
INSERT [dbo].[ReportDetails] ([Id], [TestId], [TestName], [NormalMinValue], [NormalMaxValue], [ResultedValue]) VALUES (134, 47, N'Urin Test', 30, 40, 35)
SET IDENTITY_INSERT [dbo].[ReportDetails] OFF
SET IDENTITY_INSERT [dbo].[SortedData] ON 

INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (14, 53.3333333333333, 1)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (15, -22.3214285714286, 2)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (16, 92, 3)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (17, -30, 4)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (18, 53.3333333333333, 5)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (19, 0, 6)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (22, 0, 9)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (23, 5.88235294117648, 10)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (24, -73.6111111111111, 11)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (25, 25, 12)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (26, 5.98290598290596, 13)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (27, -6, 14)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (28, 1.92307692307693, 15)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (29, 53.3333333333333, 65)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (30, -22.3214285714286, 66)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (31, 92, 67)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (32, -30, 68)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (33, 0, 69)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (34, 0, 70)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (35, 5.88235294117648, 71)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (36, -73.6111111111111, 72)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (37, 25, 73)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (38, -6, 74)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (39, 1.92307692307693, 75)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (40, -80, 76)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (41, 16.6666666666667, 77)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (42, 18.0327868852459, 78)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (43, -45.4545454545455, 79)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (44, -18.4444444444445, 80)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (45, -56.6666666666667, 81)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (46, -50, 82)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (47, -20, 83)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (48, -25, 84)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (49, -84.7916666666667, 85)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (50, -81.6842105263158, 86)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (51, -75.8823529411765, 87)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (52, -91.4285714285714, 88)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (53, 1.7391304347826, 89)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (54, -76.4285714285714, 90)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (55, 14.018691588785, 91)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (56, -20, 92)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (57, 66.2027833001988, 93)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (58, 50, 94)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (59, 51.1111111111111, 95)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (60, -9.09090909090909, 96)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (61, 2.22222222222223, 97)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (62, -70, 98)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (63, -62.5, 99)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (64, 9.09090909090909, 100)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (65, -8.33333333333334, 101)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (66, -85.4166666666667, 102)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (67, 0, 103)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (68, 15.211970074813, 104)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (69, -91.4285714285714, 105)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (70, 51.9565217391304, 106)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (71, 72, 107)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (72, 18.5840707964602, 108)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (73, -20, 109)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (74, 6.66666666666667, 110)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (75, 0, 111)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (76, 60, 112)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (77, 57.1428571428572, 113)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (78, 26.0869565217391, 116)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (79, 10, 117)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (80, 26.6666666666667, 118)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (81, 50, 119)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (82, -76.0869565217391, 120)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (83, -75, 121)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (84, 20, 122)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (85, 33.3333333333333, 123)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (86, -66.6666666666667, 124)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (87, 20, 125)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (88, 3.33333333333334, 126)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (89, 16.6666666666667, 128)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (90, -63.3333333333333, 129)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (91, 0, 130)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (92, 18.5840707964602, 131)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (93, 5.88235294117646, 133)
INSERT [dbo].[SortedData] ([Id], [ResultedDiff], [ReportId]) VALUES (94, -75, 134)
SET IDENTITY_INSERT [dbo].[SortedData] OFF
SET IDENTITY_INSERT [dbo].[TestImage] ON 

INSERT [dbo].[TestImage] ([Id], [ImagePath], [ReportId]) VALUES (1, N'/Content/image/blood-test-results-acid-blood.jpg', 1)
INSERT [dbo].[TestImage] ([Id], [ImagePath], [ReportId]) VALUES (2, N'/Content/image/blood-test-results-mold.jpg', 10)
SET IDENTITY_INSERT [dbo].[TestImage] OFF
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (1, CAST(0x0000A87B00000000 AS DateTime), 12)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (1, CAST(0x0000A89500000000 AS DateTime), 13)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (1, CAST(0x0000A8AE00000000 AS DateTime), 14)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (1, CAST(0x0000A8B300000000 AS DateTime), 15)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (1, CAST(0x0000A8CD00000000 AS DateTime), 16)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (2, CAST(0x0000A85B00000000 AS DateTime), 17)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (2, CAST(0x0000A87200000000 AS DateTime), 18)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (2, CAST(0x0000A8D400000000 AS DateTime), 19)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (2, CAST(0x0000A88D00000000 AS DateTime), 20)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (2, CAST(0x0000A96E00000000 AS DateTime), 21)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (3, CAST(0x0000A8AE00000000 AS DateTime), 22)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (3, CAST(0x0000A8B300000000 AS DateTime), 23)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (3, CAST(0x0000A8CD00000000 AS DateTime), 24)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (3, CAST(0x0000A85F00000000 AS DateTime), 25)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (3, CAST(0x0000A8E100000000 AS DateTime), 26)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (1, CAST(0x0000A8D200000000 AS DateTime), 27)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (1, CAST(0x0000A89A00000000 AS DateTime), 28)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (1, CAST(0x0000A8E600000000 AS DateTime), 29)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (1, CAST(0x0000A70100000000 AS DateTime), 30)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (1, CAST(0x0000A70A00000000 AS DateTime), 31)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (1, CAST(0x0000A80000000000 AS DateTime), 32)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (2, CAST(0x0000A72000000000 AS DateTime), 33)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (2, CAST(0x0000A6F000000000 AS DateTime), 34)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (2, CAST(0x0000A73A00000000 AS DateTime), 35)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (2, CAST(0x0000A74500000000 AS DateTime), 36)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (2, CAST(0x0000A80200000000 AS DateTime), 37)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (2, CAST(0x0000A75B00000000 AS DateTime), 38)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (2, CAST(0x0000A76400000000 AS DateTime), 39)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (3, CAST(0x0000A7C600000000 AS DateTime), 40)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (3, CAST(0x0000A77800000000 AS DateTime), 41)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (3, CAST(0x0000A78200000000 AS DateTime), 42)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (3, CAST(0x0000A78A00000000 AS DateTime), 43)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (3, CAST(0x0000A79400000000 AS DateTime), 44)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (3, CAST(0x0000A7A000000000 AS DateTime), 45)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (1, CAST(0x0000A7A900000000 AS DateTime), 46)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (1, CAST(0x0000A7B000000000 AS DateTime), 47)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (2, CAST(0x0000A7BD00000000 AS DateTime), 48)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (2, CAST(0x0000A82500000000 AS DateTime), 49)
INSERT [dbo].[YearlyTest] ([PatientId], [TestDate], [TestId]) VALUES (3, CAST(0x0000A7DB00000000 AS DateTime), 50)
USE [master]
GO
ALTER DATABASE [medicaldata] SET  READ_WRITE 
GO
